##### 快速登陆工具

[TOC]

###### 启动项目：

* 拉取代码 ，替换相应版本的 driver
* ` python3 fastLogin/src/fastLogin.py`



###### 项目计划：

 - [x] selenium启动浏览器登陆并跳转方法
 - [x] UI窗口启动
 - [x] 账户添加
 - [x] 替换成接口获取cookie后set进driver,登陆速度会比现在快
 - [ ] selenium driver自动匹配下载
 - [ ] 更新本地文件之后如何实现刷新界面（添加/修改/删除用户后刷新）
 - [ ] 绘制页面布局：列表：角色、账户、查看、编辑
 - [ ] 瀑布流展示账户内容
 - [ ] 圭臬隔离（设置cookie，从圭臬接口获取隔离组信息）
 - [ ] 新开弹窗查看：账户、密码、角色、环境，跳转地址，编辑带删除按钮
 - [ ] 账户是否能登陆校验
 - [ ] 账户修改
 - [ ] 账户删除
 - [ ] 账户排序



###### 1.背景

测试过程1中需要频繁切换登陆不同的身份，登出登陆、切换浏览器身份等操作耗，测试账户/密码有时候也记不住。



###### 2.想法

通过selenium操控浏览器自动登录，并跳转到我们常用的页面，或者打开多个常用的页面，方便我们测试。



###### 3.计划实现功能

> 1.通过pyqt5绘制UI操作界面
>
> 2.通过python+selenium实现UI自动化登录、跳转



###### 4.项目地址(fastLogin)

Clone with SSH：git@gitlab.com:nicaicai/fastlogin.git
Clone with HTTPS：https://gitlab.com/nicaicai/fastlogin.git


###### 5.项目启动

* [谷歌浏览器驱动下载](http://npm.taobao.org/mirrors/chromedriver/) 找到和你chorme版本匹配的驱动，下载并替换到项目的resource路径下
* 进入项目src目录下
* ```python3 qy.py```启动项目



###### 6.项目开始之前需要了解

* [Python3基础 ](https://www.runoob.com/python3/python3-tutorial.html)
* [PyQt5](http://code.py40.com/pyqt5/16.html)   (Python的一个GUI库，用于绘制界面)
* [谷歌浏览器驱动下载](http://npm.taobao.org/mirrors/chromedriver/)



###### 7.项目计划

> 一期功能：

<img src="/Users/nicaicai/Library/Application Support/typora-user-images/image-20210108110038533.png" alt="image-20210108110038533" style="zoom:50%;" />




> json例子

```json
[
    {
        "username":"admin",
        "password":"test123456",
        "type":0,
        "env":0,
        "preset_url":"http://jinrong.test.zcygov.cn/luban/test/loan"
    },
    {
        "username":"yuzhou112505",
        "password":"test123456",
        "type":1,
        "env":1,
        "preset_url":"https://jinrong-staging.zcygov.cn/luban/test/loan"
    }
]
```

> 字段解释

|  字段 | 类型 | 解释 | 举例 |
|  ----  | ----  |  ----  |  ----  |
| username | String | 账户名 | admin |
| password | String | 账户对应的密码 | test123456 |
| type | Int | 账户类型 | 0:运营、1:供应商、2:采购单位 |
| env | Int | 账户环境 | 0:测试环境、1:预发环境、2:生产环境 |
| preset_url | string | 登陆后跳转到该地址 | "http://jinrong.test.zcygov.cn/luban/test/loan" |




