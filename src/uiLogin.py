# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-11 16:29:39
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-08 11:01:40
from selenium import webdriver
import os
import time
from userCenter import UserCenter


def login(env, username, password, preset_url):
    # base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # 驱动 是否能识别但前系统，匹配不同的驱动？
    # windows驱动
    # chromedriver = "./chromedriver.exe"
    # mac 驱动
    chromedriver = "./chromedriver"
    os.environ["webdriver.chrome.driver"] = chromedriver

    driver = webdriver.Chrome(chromedriver)
    driver.maximize_window()
    # 登陆zcy地址
    # test
    url_test = 'http://login.test.zcygov.cn/user-login/#/login'
    # staging
    url_staging = 'https://login-staging.zcygov.cn/user-login/#/login'
    if env == 0:
        driver.get(url_test)
        env = 'test'
    elif env == 1:
        driver.get(url_staging)
        env = 'staging'
    else:
        print('暂时不支持该环境')

    # driver.find_element_by_xpath('//*[@id="username"]').send_keys(username)
    # driver.find_element_by_xpath('//*[@id="password"]').send_keys(password)
    # driver.find_element_by_xpath('//*[@id="root"]/div/div/div[2]/div[2]/div/div/div/div[1]/div/form/button').click()

    # guinie_url_1 = 'http://guinie.test.zhengcaiyun.cn/r?a=guinie-isolation-33'
    # guinie_url_2 = 'http://guinie.test.zcygov.cn/r?a=guinie-isolation-33'

    # 调用用户中心的接口获取cookie设置到driver里面
    user = UserCenter(username, password, env)
    cookies = user.get_cookies()
    for i in cookies:
        user_cookie = {'name': i.name, 'value': i.value}
        driver.add_cookie(user_cookie)
    driver.get(preset_url)

    # 设置圭臬灰度cookie
    # guinie_cookie = {'name': 'guinie_gray', 'value': 'guinie-isolation-9'}
    # driver.add_cookie(guinie_cookie)


if __name__ == '__main__':
    env = 1
    username = 'shzem'
    password = 'test123456'
    preset_url = 'https://middle-staging.zcygov.cn/integrity/#/credit/dashboard?_app_=zcy.credit'
    login(env, username, password, preset_url)
