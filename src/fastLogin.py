# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-11 16:27:06
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-08 10:34:51
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtGui import QColor
from PyQt5 import QtGui
from PyQt5.Qt import QInputDialog
from uiLogin import login
from user import User
from functools import partial
from yzDialog import addUserDlg
import time


class Button(QPushButton):
    def __init__(self, title, parent):
        super().__init__(title, parent)


class FastLogin(QWidget):
    '''[快速登陆窗口类]
    Extends:
        QWidget
    '''

    def __init__(self):
        super().__init__()
        self.title = '快速登陆'
        self.left = 50
        self.top = 100
        self.width = 300
        self.height = 600
        self.initUI()

    # 初始化UI
    def initUI(self):
        # 窗口位置和大小、标题、图标
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        palette1 = QtGui.QPalette()
        palette1.setColor(self.backgroundRole(), QColor(48, 65, 86))  # 背景颜色
        self.setPalette(palette1)
        self.setAutoFillBackground(True)

        # 维护一个本地文件去获取测试账户
        user = User()
        users = user.get_user()

        # 添加用户按钮
        self.button_add = QPushButton('添加测试用户', self)
        self.button_add.setToolTip("添加测试用户")
        self.button_add.move(0, 0)
        self.button_add.setFixedSize(300, 42)
        self.setButtonStyle(self.button_add)
        self.button_add.clicked.connect(self.AddUserDlg)

        # 遍历展示测试账户按钮
        for index, user in enumerate(users):
            button = QPushButton(user['username'], self)
            button.setToolTip("去快速登陆政采云")
            button.move(0, 60 + 42 * index)
            button.setFixedSize(300, 42)
            self.setButtonStyle(button)

            # lambda 函数传递变量地址（无效）
            # button.clicked.connect(lambda: self.on_click(user['username'], user['password']))
            # partial 偏函数实时传递值（花了一天时间，记录一下）
            button.clicked.connect(partial(self.on_click, user['env'],
                                           user['username'], user['password'], user['preset_url']))

    # 添加用户弹窗
    def AddUserDlg(self):
        dialog = addUserDlg(self)
        if dialog.exec_():  # accept 则返回1，reject返回0
            # 选择环境
            if dialog.env.currentText() == "测试":
                env = 0
            elif dialog.env.currentText() == "预发":
                env = 1
            else:
                print('环境参数错误！')
            # 暂时默认添加供应商账户
            User.add_user(self, dialog.username.text(), dialog.password.text(), 1, env, dialog.url.text())

    # 删除用户
    def delBtn(self, index):
        print("删除操作//todo" + str(index))

    # 去登陆
    def on_click(self, env, username, password, preset_url):
        # 调用ui自动化
        login(env, username, password, preset_url)

    # 公共设置css的方法
    def setButtonStyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:rgb(191, 203, 217)}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(48, 65, 86)}"  # 背景色
            "QPushButton:hover{background:#2d3a4b}"  # 按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            "QPushButton:pressed{background-color:#2d3a4b}"  # 按下时的样式
        )


if __name__ == '__main__':
    app = QApplication(sys.argv)
    fastLogin = FastLogin()
    fastLogin.show()
    sys.exit(app.exec_())
