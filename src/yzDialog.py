# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-17 16:13:08
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-08 10:31:39
import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QLabel, QComboBox, QLineEdit, QDialogButtonBox, QGridLayout, QDialog
from user import User


class addUserDlg(QDialog):
    '''[添加用户弹窗]
    Extends:
        QDialog
    '''

    def __init__(self, parent=None):
        super().__init__(parent)

        # 环境选择
        envlable = QLabel("环境：")
        self.env = QComboBox()
        self.env.addItems(['测试', '预发'])

        # 输入账号
        namelable = QLabel("账号:")
        self.username = QLineEdit()

        # 输入密码
        passlable = QLabel("密码:")
        self.password = QLineEdit()

        # 输入常用地址
        urllable = QLabel("常用地址链接:")
        self.url = QLineEdit()

        # 确定取消按钮，绑定事件
        buttonBox = QDialogButtonBox()
        buttonBox.addButton("确定", QDialogButtonBox.AcceptRole)
        buttonBox.addButton("取消", QDialogButtonBox.RejectRole)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        # 创建布局
        layout = QGridLayout()

        # 布局环境选项
        layout.addWidget(envlable, 0, 0)
        layout.addWidget(self.env, 0, 1)
        # 布局用户名
        layout.addWidget(namelable, 1, 0)
        layout.addWidget(self.username, 1, 1)
        # 布局密码
        layout.addWidget(passlable, 2, 0)
        layout.addWidget(self.password, 2, 1)
        # 布局常用地址链接
        layout.addWidget(urllable, 3, 0)
        layout.addWidget(self.url, 3, 1)
        # 布局确定取消按钮
        layout.addWidget(buttonBox, 4, 1)

        # 设置dialog布局
        self.setLayout(layout)
        self.setWindowTitle("添加测试用户")
