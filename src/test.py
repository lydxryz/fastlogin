import json


class User():
    # def __init__(self):
        # self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    def get_user(self):  # 读取测试账户信息
        with open("src/user.json", 'r') as user_file:
            user_dict = json.load(user_file)
            # print(user_dict)
            return user_dict

    def add_user(self, username, password, type, env, preset_url):  # 新增测试账户信息
        with open("src/user.json", 'r') as user_file:
            user_dict = json.load(user_file)
            new_user = {}
            new_user['username'] = username
            new_user['password'] = password
            new_user['type'] = type
            new_user['env'] = env
            new_user['preset_url'] = preset_url
            user_dict.append(new_user)
        file = open("src/user.json", 'w')
        js = json.dumps(user_dict, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
        file.write(js)
        file.close()

    def del_user(self,index):  # 删除用户
        with open("src/user.json", 'r') as user_file:
            user_dict = json.load(user_file)
            print(user_dict)
            user_dict.pop(index)
        file = open("src/user.json", 'w')
        js = json.dumps(user_dict, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
        file.write(js)
        file.close()



if __name__ == '__main__':
    user = User()
    user.get_user()
    user.del_user()

    # user.add_user('t', 'w', 1, 1, 'http://jinrong.test.zcygov.cn/luban/test/loan')
    # user.get_user()